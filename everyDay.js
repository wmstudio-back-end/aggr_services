module.exports.everyDay              = everyDay
function everyDay(){
    var time = parseInt(new Date().getTime() / 1000);

        //отключение от тарифа по ограниченному времени
        dataBase.collection("users").aggregate([

            {
                $match: { "type_profile": "Company","Company.auth":true }
            },
            // { $slimit: 1 },
            {
                $lookup: {
                    from: "user_order",
                    localField: "service.active.orderId",
                    foreignField: "_id",
                    as: "user_order",
                }
            },
            {
                $match: {
                    "user_order": { $ne: [] } ,
                    "user_order.service.subscription": { $ne: null } ,
                    "user_order.service.subscription.deactivate_at": { $ne: 0 } ,

                }
            },
            {
                $project:{
                    "service":1,
                    "user_order":1
                }
            }
        ]).forEach(function (result) {

            //прошло время действия оплаченного пакета
            if (
                result.user_order[0].service.subscription.deactivate_at>0
                &&result.user_order[0].service.subscription.deactivate_at<time
                &&result.user_order[0].status==1
            ){
                cancellation(dataBase,result._id)
                console.log('ПОРА СПИСЫВАТЬ');
            }

        })



}

function cancellation(db,uid){
    var data = {
        "orderId" : null,
        "packageId" : null,
    }
    db.collection("users").update({"_id":ObjectId(uid)},{$set:{"service.active.orderId":null,"service.active.packageId":null}},function(err,res){
        console.log(err)
        console.log(res)
        console.log("UID",uid)
        //console.log(new Date(data[0].service.subscription.deactivate_at*1000))
    })


}
