
argv = require('minimist')(process.argv.slice(2));
console.log(argv)

global.config = global.argv
/*
 Seconds: 0-59
 Minutes: 0-59
 Hours: 0-23
 Day of Month: 1-31
 Months: 0-11
 Day of Week: 0-6
 * */
mongoClient = require("mongodb").MongoClient;
ObjectId = require('mongodb').ObjectID;

request = require('request');
var http = 'http'
if (global.config.https){
    http = 'https'
}


var everyDay = require('./everyDay').everyDay;
var everyMonth = require('./everyMonth').everyMonth;
var CronJob = require('cron').CronJob;
dataBase = null
var url = "mongodb://project:project@localhost:27017/project";
mongoClient.connect(url, function(err, db) {
    console.log(err)
    dataBase = db
     everyMonth()

    everyDay()



var jobDay = new CronJob({
    cronTime: '* 0 * * * *',
    onTick: everyDay,
    start: false,
    timeZone: 'America/Los_Angeles'
});
var jobMonth = new CronJob({
    cronTime: '* * * 1 * *',
    onTick: everyMonth,
    start: false,
    timeZone: 'America/Los_Angeles'
});

 jobDay.start();
 jobMonth.start();


})

